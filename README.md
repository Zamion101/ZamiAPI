[![](https://jitpack.io/v/com.gitlab.Zamion101/ZamiAPI.svg)](https://jitpack.io/#com.gitlab.Zamion101/ZamiAPI)


Add to repositories!

```
    <repositories>
        ...
		<repository>
		    <id>jitpack.io</id>
		    <url>https://jitpack.io</url>
		</repository>
		...
	</repositories>
```

Add to dependencies!

```
<dependencies>
    ...
    <dependency>
	    <groupId>com.gitlab.Zamion101</groupId>
	    <artifactId>ZamiAPI</artifactId>
	    <version>v1.0.0</version>
	</dependency>
	...
</dependencies>


```