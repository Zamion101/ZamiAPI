package me.Zamion101.ZamiAPI.utils;

import java.util.Random;

public class RandomStringGenerator {

    private static final String CHAR_LIST = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!?*&%$#";

    public static String generateRandomString(int LENGTH){
        StringBuffer randStr = new StringBuffer();
        for(int i=0; i<LENGTH; i++){
            int number = getRandomNumber(CHAR_LIST);
            char ch = CHAR_LIST.charAt(number);
            randStr.append(ch);
        }
        return randStr.toString();
    }

    public static String generateRandomString(String CHAR_LIST,int LENGTH){
        StringBuffer randStr = new StringBuffer();
        for(int i=0; i<LENGTH; i++){
            int number = getRandomNumber(CHAR_LIST);
            char ch = CHAR_LIST.charAt(number);
            randStr.append(ch);
        }
        return randStr.toString();
    }

    private static int getRandomNumber(String CHAR_LIST) {
        int randomInt = 0;
        Random randomGenerator = new Random();
        randomInt = randomGenerator.nextInt(CHAR_LIST.length());
        if (randomInt - 1 == -1) {
            return randomInt;
        } else {
            return randomInt - 1;
        }
    }

}
