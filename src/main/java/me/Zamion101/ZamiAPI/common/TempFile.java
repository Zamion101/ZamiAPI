package me.Zamion101.ZamiAPI.common;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class TempFile {

    private static Map<String,File> files = new HashMap<String, File>();

    public static boolean newFile(String identifier){
        return newFile(identifier,".ztmp");
    }

    public static boolean newFile(String identifier,String extension){
        if(!validate(identifier))return false;
        File t = null;
        try {
            t = File.createTempFile("temp_" + identifier + "-", extension);
            files.put(identifier,t);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

    }

    public static boolean removeFile(String identifier){
        if(!validate(identifier)) return false;
        File t = files.get(identifier);
        files.remove(identifier);
        return t.delete();
    }

    public static File getTempFile(String identifier){
        if(validate(identifier)) return null;
        return files.get(identifier);
    }

    public static File timeNamedTempFile(String extension){
        try {
            return File.createTempFile(String.valueOf(System.currentTimeMillis()),extension);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static File timeNamedTempFile(){
        return timeNamedTempFile(".ztmp");
    }

    private static boolean validate(String identifier){
        boolean a,b,c;
        a = (identifier != null);
        b = (!identifier.equalsIgnoreCase(""));
        c = !files.containsKey(identifier);
        return a && b && c;
    }

    public static void clear(){
        if(files.size() == 0) return;
        for(String s : files.keySet()){
            File file = files.get(s);
            file.delete();
            files.remove(s);
            System.out.println(s);
            System.out.println(file.getName());
            System.out.println(file);
        }
    }

}
