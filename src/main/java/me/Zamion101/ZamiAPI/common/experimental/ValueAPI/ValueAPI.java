package me.Zamion101.ZamiAPI.common.experimental.ValueAPI;

import java.util.HashMap;
import java.util.Map;

public class ValueAPI{

    private static Map<String,Value<?>> valueMap = new HashMap<String, Value<?>>();

    public static void newValue(Value<?> value){
        if(existPath(value.getPath())) return;
        if(existValue(value)) return;

        valueMap.put(value.getPath(),value);
    }

    public static void replaceValue(String path,Value<?> value){
        if(!existPath(path)) return;

        valueMap.put(path,value);
    }

    public static void rePath(String oldPath,String newPath){
        if(!existPath(oldPath))return;
        if(existPath(newPath)) return;

        Value<?> val = valueMap.get(oldPath);
        valueMap.remove(oldPath);
        valueMap.put(newPath,val);
    }

    public static void deleteValue(String path){
        if(!existPath(path)) return;
        valueMap.remove(path);
    }

    public static boolean isPathExist(String path){
        return existPath(path);
    }

    public static boolean isValueExist(Value<?> value){
        return existValue(value);
    }

    public static <T> Value<T> getRawValue(String path){
        return (Value<T>) valueMap.get(path);
    }

    public static <T> T getValue(String path){
        Value<T> val = (Value<T>) valueMap.get(path);
        return val.returnValue();
    }

    public static boolean isValueNull(String path){
        return (getValue(path) == null);
    }



    private static boolean existValue(Value<?> value){
        return valueMap.containsValue(value);
    }

    private static boolean existPath(String path){
        return valueMap.containsKey(path);
    }
}
