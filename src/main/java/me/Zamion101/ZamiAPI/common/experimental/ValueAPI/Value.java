package me.Zamion101.ZamiAPI.common.experimental.ValueAPI;

import java.io.Serializable;

public abstract class Value<T> implements Serializable{

    public abstract T returnValue();

    private String path;

    public Value(String path){
        this.path = path;
    }

    public String getPath() {
        return path;
    }
}
