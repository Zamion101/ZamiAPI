package me.Zamion101.ZamiAPI.manager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RestUrlManager {

    private String BASE_URL;
    private Map<Integer,Object> args = new HashMap<Integer, Object>();
    private List<String> rawArgs = new ArrayList<String>();
    private Map<String,Object> rawValues = new HashMap<String,Object>();


    public RestUrlManager(String BASE_URL){
        this.BASE_URL = BASE_URL;
    }

    public RestUrlManager(){}

    public RestUrlManager setBaseUrl(String BASE_URL){
        this.BASE_URL = BASE_URL;
        return this;
    }

    public RestUrlManager addArg(String arg,Object value){
        String temp;
        if(args.size() == 0){
            temp = "?" + arg + "=" + String.valueOf(value);
        }else{
            temp = "&" + arg + "=" + String.valueOf(value);
        }
        rawArgs.add(arg);
        rawValues.put(arg,value);
        args.put(args.size(),temp);
        return this;
    }

    public RestUrlManager removeArg(int index){
        String a = rawArgs.get(index);
        rawArgs.remove(index);
        rawValues.remove(a);
        fixArgs();

        return this;
    }

    public RestUrlManager removeArg(String arg){
        for(int i = 0; i <= rawArgs.size(); i++){
            if(rawArgs.get(i).equalsIgnoreCase(arg)){
                removeArg(i);
                break;
            }else{
                continue;
            }
        }
        return this;

    }

    private void fixArgs(){
        args.clear();
        for(int i = 0; i <= rawArgs.size(); i++){
            addArg(rawArgs.get(i),rawValues.get(rawArgs.get(i)));
        }
    }

    public RestUrlManager setValue(int index,String value) {
        if(index > rawArgs.size()) return this;
        if(value == null) return this;
        setValue(rawArgs.get(index),value);
        return this;
    }

    public RestUrlManager setValue(String arg,String value){
        if(value == null) return this;
        if(rawValues.get(arg) != null){
            rawValues.put(arg,value);
        }
        return this;
    }

    public String getRestURL(){
        StringBuilder builder = new StringBuilder(BASE_URL);
        for(int i = 0; i <= args.size(); i++){
            builder.append(args.get(i));
        }
        return builder.toString();
    }


}
