package me.Zamion101.ZamiAPI.manager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ArgManager<T> {

    protected Map<Integer,T> values = new HashMap<Integer, T>();

    protected int queue = 0;

    private void init(T arg){
        values.put(length(),arg);
    }

    public ArgManager(T... args){
        for(T a : args){
            init(a);
        }
    }

    public ArgManager(List<T> args){
        new ArgManager<T>((T) args.toArray());
    }

    public int length(){
        return values.size();
    }

    public <T> T next(){
        queue += 1;
        return (T) values.get(queue - 1);
    }

    public boolean hasNext(){
        return (values.get(queue + 1) != null);
    }

}
