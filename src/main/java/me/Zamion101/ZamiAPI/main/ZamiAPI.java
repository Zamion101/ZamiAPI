package me.Zamion101.ZamiAPI.main;


import me.Zamion101.ZamiAPI.common.TempFile;
import me.Zamion101.ZamiAPI.common.experimental.ValueAPI.Value;
import me.Zamion101.ZamiAPI.common.experimental.ValueAPI.ValueAPI;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class ZamiAPI {

    private Object instance;
    private ZamiAPI apiInstance;

    public ZamiAPI(Object instance){
        this.instance = instance;
        this.apiInstance = this;

        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            public void run() {
                TempFile.clear();
            }
        }));
    }

    public static void main(String[] args){
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            public void run() {
                TempFile.clear();
            }
        }));
    }


    public <T> T getInstance(T type) {
        return (T)instance;
    }

    public Object getInstance() {
        return instance;
    }

}
